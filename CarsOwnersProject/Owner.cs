﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarsOwnersProject
{
    public class Owner
    {
        private int _num_cars;
        public int id { get; set; }

        [Required]
        [StringLength(50)]
        public string name { get; set; }

        [Required]
        public byte[] photo { get; set; }

        public int numOfCars
        {
            get
            {
                return _num_cars;
            }
            set
            {
                _num_cars = value;
            }
        }

       
        public ICollection<Car> cars { get; set; }

        

        public Owner(string name, byte[] photo)
        {
           
            this.name = name;
            this.photo = photo;
            
        }

        public Owner()
        {
            
        }

        public Owner(int numCars)
        {
            _num_cars = numCars;
        }
    }
}