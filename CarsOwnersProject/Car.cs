﻿using System.Collections.Generic;

namespace CarsOwnersProject
{
    public class Car
    {
        public int carId { get; set; }
        public string carName { get; set; }

        public int ownerId { get; set; }
        public Owner owner { get; set; }

        public Car()
        {
            
        }

        public Car(string carName, int ownerId)
        {
            
            this.carName = carName;
            this.ownerId = ownerId;
            
        }
        
    }
}