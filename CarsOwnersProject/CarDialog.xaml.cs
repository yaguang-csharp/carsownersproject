﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace CarsOwnersProject
{
    /// <summary>
    /// Interaction logic for CarDialog.xaml
    /// </summary>
    public partial class CarDialog : Window
    {
        public event Action<int> AddedNewCar;
        public Owner CurreOwner;
        static OwnerDatabaseContext odc = new OwnerDatabaseContext();
        public CarDialog(Owner owner)
        {
            
            InitializeComponent();
            CurreOwner = owner;
            List<Car> carList = (from c in odc.carSet where c.ownerId == CurreOwner.id select c).ToList();
            lvCars.ItemsSource = carList;
            lvCars.Items.Refresh();
            tbOwnerName.Text = owner.name;
            
        }


        private void btnAddCar_Click(object sender, RoutedEventArgs e)
        {
            var modelText = txtModel.Text;
            if (modelText == null || modelText.Length == 0)
            {
                MessageBox.Show("this field is mandatory", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            Car car = new Car(modelText,CurreOwner.id);
            odc.carSet.Add(car);
            odc.SaveChanges();
            /*List<Car> cars = (from c in odc.carSet where c.ownerId == CurreOwner.id select c).ToList();
            lvCars.ItemsSource = cars;
            lvCars.Items.Refresh();*/
            ResetCar();
        }

        private void btnUpdateCar_Click(object sender, RoutedEventArgs e)
        {
            if (lvCars.SelectedIndex == -1)
            {
                MessageBox.Show("You have not chosen any car", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            var selectedCar = lvCars.SelectedItem;
            if (selectedCar is Car)
            {
                Car car = (Car) selectedCar;
                car.carName = txtModel.Text;
                odc.SaveChanges();
                ResetCar();
                /*List<Car> carList = (from c in odc.carSet where c.ownerId == CurreOwner.id select c).ToList();
                lvCars.ItemsSource = carList;
                lvCars.Items.Refresh();
                tbCarId.Text = "...";
                txtModel.Clear();
                lvCars.SelectedIndex = -1;*/
            }
        }

        private void lvCars_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnDeleteCar.IsEnabled = true;
            btnUpdateCar.IsEnabled = true;

            var selectedCar = lvCars.SelectedItem;
            if (selectedCar is Car)
            {
                Car car = (Car) selectedCar;
                tbCarId.Text = car.carId.ToString();
                txtModel.Text = car.carName;
            }
        }

        private void btnDeleteCar_Click(object sender, RoutedEventArgs e)
        {
            if (lvCars.SelectedIndex == -1)
            {
                MessageBox.Show("You have not chosen any car", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            var selectedCar = lvCars.SelectedItem;
            if (selectedCar is Car)
            {
                Car car = (Car)selectedCar;
                odc.carSet.Remove(car);
                odc.SaveChanges();
                ResetCar();
            }
        }

        private void ResetCar()
        {
            List<Car> carList = (from c in odc.carSet where c.ownerId == CurreOwner.id select c).ToList();
            lvCars.ItemsSource = carList;
            lvCars.Items.Refresh();
            tbCarId.Text = "...";
            txtModel.Clear();
            lvCars.SelectedIndex = -1;
        }

        private void btnDone_Click(object sender, RoutedEventArgs e)
        {
            List<Car> carList = (from c in odc.carSet where c.ownerId == CurreOwner.id select c).ToList();
            AddedNewCar?.Invoke(carList.Count);
            //MessageBox.Show(carList.Count.ToString());
            DialogResult = true;
        }
    }
}
