﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsOwnersProject
{
    class OwnerDatabaseContext:DbContext
    {
        const string databasename = "ownerDatabase4.mdf";
        private static string DbPath = Path.Combine(Environment.CurrentDirectory, databasename);

        public OwnerDatabaseContext() : base($@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={DbPath};Integrated Security=True;Connect Timeout=30")
        {
        }

        public DbSet<Owner> ownerSet { get; set; }
        public DbSet<Car> carSet { get; set; }
        public object Tables { get; internal set; }
    }
}