﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Microsoft.Win32;

namespace CarsOwnersProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static OwnerDatabaseContext ownerdc = new OwnerDatabaseContext();
        OpenFileDialog ofd = new OpenFileDialog();
        public MainWindow()
        {
            InitializeComponent();
            LoadDataFromDataBase();
        }

        private void LoadDataFromDataBase()
        {
            //MessageBox.Show(ownerdc.ownerSet.ToString());
            List<Owner> ownerList = (from o in ownerdc.ownerSet select o).ToList();
            lvPerson.ItemsSource = ownerList;

        }


        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            string nameText = txtName.Text;
            if (nameText.Length == 0 || nameText == null)
            {
                MessageBox.Show("Owner name is mandatory field.", "Warning", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }
            
            try
            {
                byte[] imageData = ImageToByte(new BitmapImage(new Uri(ofd.FileName)));
               
                Owner owner = new Owner(nameText, imageData);
                ownerdc.ownerSet.Add(owner);
                ownerdc.SaveChanges();

                /*List<Owner> ownerList = (from o in ownerdc.ownerSet select o).ToList();
                lvPerson.ItemsSource = ownerList;
                txtName.Clear();
                ownerImg.Source = null;*/
                ResetData();

            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void BtnImg_OnClick(object sender, RoutedEventArgs e)
        {
            ofd.DefaultExt = "JPEG Files (*.jpeg)|*.jpeg|PNG Files(*.png)|*.png|JPG Files|(*.jpg)|*.jpg|GIF Files(*.gif)|*.gif";
            ofd.Title = "Select a picture";
            if (ofd.ShowDialog() == true)
            {
                ownerImg.Source = new BitmapImage(new Uri(ofd.FileName));
            }
        }


        private void lvPerson_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnCars.IsEnabled = true;
            btnDelete.IsEnabled = true;
            btnUpdate.IsEnabled = true;

            var selectedItem = lvPerson.SelectedItem;
            if (selectedItem is Owner)
            {
                Owner owner = (Owner) selectedItem;
                tbId.Text = owner.id.ToString();
                txtName.Text = owner.name;
                /*MessageBox.Show(owner.photo.GetType().ToString());*/
                byte[] imageData = owner.photo;
                var loadImage = LoadImage(imageData);
                ownerImg.Source = loadImage;
            }
        }

        private static BitmapImage LoadImage(byte[] imageData)
        {
            if (imageData == null || imageData.Length == 0)
            {
                return null;
            }
            var image = new BitmapImage();
            using (var mem = new MemoryStream(imageData))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            return image;

        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (lvPerson.SelectedIndex == -1)
            {
                MessageBox.Show("You have not chosen any owner.", "Warning", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            var selectedItem = lvPerson.SelectedItem;
            if (selectedItem is Owner)
            {
                Owner owner = (Owner) selectedItem;
                owner.name = txtName.Text;
                ownerImg.Source = new BitmapImage(new Uri(ofd.FileName));
                byte[] imageBytes = ImageToByte(new BitmapImage(new Uri(ofd.FileName)));
                owner.photo = imageBytes;
               
                ownerdc.SaveChanges();
            }

            ResetData();

        }

        private static byte[] ImageToByte(BitmapImage imageSource)
        {
            byte[] data;
            BitmapEncoder encoder = new BmpBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(imageSource));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }

            return data;
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lvPerson.SelectedIndex == -1)
            {
                MessageBox.Show("You have not chosen any owner", "Warning", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            var selectedItem = lvPerson.SelectedItem;
            if (selectedItem is Owner)
            {
                Owner owner = (Owner) selectedItem;
                ownerdc.ownerSet.Remove(owner);
                ownerdc.SaveChanges();
            }
            ResetData();
        }

        private void btnCars_Click(object sender, RoutedEventArgs e)
        {
            if (lvPerson.SelectedIndex == -1)
            {
                MessageBox.Show("you have not chosen any owner", "Warning", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            var selectedItem = lvPerson.SelectedItem;
            if (selectedItem is Owner)
            {
                Owner owner = (Owner) selectedItem;

                CarDialog carDialog = new CarDialog(owner);
                int num = 0;
                carDialog.Owner = this;
                carDialog.AddedNewCar += n => { num = n;};
               

                bool? result = carDialog.ShowDialog();
               

                if (result == true)
                {
                    owner.numOfCars = num;
                    ownerdc.SaveChanges();
                    
                    
                }
                ResetData();
            }
        }

        private void ResetData()
        {
            lvPerson.ItemsSource = ownerdc.ownerSet.ToList();
            txtName.Clear();
            tbId.Text = "...";
            ownerImg.Source = null;
            lvPerson.SelectedIndex = -1;
        }
    }
}
